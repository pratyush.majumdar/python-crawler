# Python Crawler
A Python plus Scrappy project to create a Web Crawler.

## Install pip
```python
https://files.pythonhosted.org/packages/11/b6/abcb525026a4be042b486df43905d6893fb04f05aac21c32c638e939e447/pip-9.0.1.tar.gz
tar -xvf pip-9.0.1.tar.gz
cd pip-9.0.1.tar.gz
sudo python setup.py install
```

## Upgrade pip (Had to upgrade to install Virtual Environment Module)
```python
pip install --upgrade pip
```

## Install Virtual Environment
```python
pip install virtualenv
```

## Create Virtual Environment inside the project directory
```python
python -m virtualenv venv
```

### Get inside the Virtual Environment
```python
source venv/bin/activate
```

### Check environment Python Version
```python
python --version
```

### Create a requirements.txt (Initially it will be empty)
```python
pip freeze > requirements.txt
```

### Exit the Virtual Environment
```python
deactivate
```

## Reinstall Environment and Modules after cloning
```python
python -m virtualenv venv
pip install -r requirements.txt
```

## Other important commands
Uninstall only removes the module, but not the dependencies, dependencies needs to be removed manually. However before removing the dependencies we need to make sure that those dependencies are not in use by other modules or the source code.
```python
pip uninstall <module_name>
pip show <module_name>
```
